import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from './Home';
import PrixScreen from './Prix';
import EvenementsScreen from './Evenements';
import CalendrierScreen from './Calendrier';
import PhotosScreen from './Photos';
import MembresScreen from './Membres';
import Evenementsoucis from './Evenementsoucis';
import {Vibration} from "react-native";
import ListeScreen from './eventListe';
import DetailScreen from './eventDetail';

const Tab = createBottomTabNavigator();

const vibratoOnTapPress = () =>({
    tabPress: () => {
        Vibration.vibrate(20);
        },
    }
);

const Navig = () => {
    return ( 
        <NavigationContainer>
            <Tab.Navigator>
                <Tab.Screen 
                    name="Home" 
                    component={HomeScreen} 
                    // color="#841584"
                    // textAlign= "center"
                    options={{
                        tabBarLabel: 'Home'
                        // color:"#841584"
                        // tabStyle:color="#841584"
                    }}
                    listeners={vibratoOnTapPress}
                />
                <Tab.Screen 
                    name="Prix" 
                    component={PrixScreen} 
                    options={{
                        tabBarLabel: 'Prix'}}
                        listeners={vibratoOnTapPress}
                />
                <Tab.Screen 
                    name="Calendrier" 
                    component={CalendrierScreen} 
                    options={{
                        tabBarLabel: 'Calendrier'}}
                        listeners={vibratoOnTapPress}
                />
                <Tab.Screen 
                    name="Evenements" 
                    component={EvenementsScreen} 
                    options={{
                        tabBarLabel: 'Evenements'}}
                        listeners={vibratoOnTapPress}
                />
                <Tab.Screen 
                    name="Evenements2" 
                    component={Evenementsoucis} 
                    options={{
                        tabBarLabel: 'Evenements2'}}
                        listeners={vibratoOnTapPress}
                />
                <Tab.Screen 
                    name="Photos" 
                    component={PhotosScreen} 
                    options={{
                        tabBarLabel: 'Photos'}}
                        listeners={vibratoOnTapPress}
                />
                <Tab.Screen 
                    name="Membres" 
                    component={MembresScreen} 
                    options={{
                        tabBarLabel: 'Membres'}}
                        listeners={vibratoOnTapPress}
                />
                <Tab.Screen 
                    name="Liste évènements" 
                    component={ListeScreen} 
                    options={{
                        tabBarLabel: 'Liste évènements'}}
                        listeners={vibratoOnTapPress}
                />    
                <Tab.Screen 
                    name="Détail évènement" 
                    component={DetailScreen} 
                    options={{
                        tabBarLabel: 'Détail évènement'}}
                        listeners={vibratoOnTapPress}
                />  
                    
            </Tab.Navigator>
        </NavigationContainer>
  );
};

export default Navig;