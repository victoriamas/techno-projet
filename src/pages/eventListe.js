import React , { useState ,useEffect} from 'react';
import { View, Text ,Image,Button,FlatList,StyleSheet,Modal,Alert} from 'react-native';
import logo from './../img/logo.png';
//import './App.css';
import './eventDatabase';
//import create from './eventDatabase';
//import { openDatabase } from 'react-native-sqlite-storage';
import Evenements from './evenement.json'

// var db = openDatabase({ name: 'SQLite' });


// const liste_event = () => { 
//   let [Evenement,setEvenement] = useState([]);
//   useEffect(() => {
//     db.transaction((tx) => {
//         tx.executeSql(
//           'SELECT * FROM evenement',
//           [],
//           (tx, results) => {
//             var event = [];
//             for (let i = 0; i < results.rows.length; ++i)
//               event.push(results.rows.item(i));
//             console.log(event); 
//             //console.log(event[2]);
//             setEvenement(event);
//           }
        
//         );
//         //console.log(evenement)
//         //console.log(Evenement)
//       });
//     }, []);
//     return (Evenement)
// }

function ListeScreen() {
    //create()
    //var event=liste_event();
    
    var events=Evenements
    console.log(events)
    const renderItem = ({ item}) => (
      <Text style={{fontWeight: 'bold',fontSize: 20}}>{item.event_id}-{item.name}</Text>
    );
    return(
        
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ fontSize: 24, textAlign: "center",fontStyle: 'italic',fontWeight: 'bold'}} >Liste des évènements </Text>
            <Image source={logo} style={{width: 100, height: 100}} />
            <FlatList
                data={events}
                renderItem={renderItem}
                keyExtractor={item => item.event_id}
                // renderItem={({events})=>(
                // <Text>
                //     {events.event_id} - {events.name}
                // </Text>)} 
            />
        </View>        
          
       );
     

}


export default ListeScreen;