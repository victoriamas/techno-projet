//import React from 'react';
//import { View, Text ,SafeAreaView,TouchableOpacity} from 'react-native';
import SQLite from "react-native-sqlite-storage";

var db = SQLite.openDatabase(
    {
      name: 'SQLite',
      location: 'default',
    },
    () => { console.log("Hello")},
    error => {
      console.log("ERROR: " + error);
    }
);


export default function create() {
    const createTable = () => {
        db.transaction(function (txn) {
          txn.executeSql('DROP TABLE IF EXISTS evenement', []);//à voir pour améliorer la création sans supprimer la table au début
          txn.executeSql(
            "SELECT name FROM sqlite_master WHERE type='table' AND name='evenement'",
            [],
            function (tx, res) {
              console.log('table_event:', res.rows.length);
              if (res.rows.length == 0) {
                txn.executeSql('DROP TABLE IF EXISTS evenement', []);
                txn.executeSql(
                  'CREATE TABLE IF NOT EXISTS evenement(event_id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(100), nb_staff INTEGER, lieu VARCHAR(200))',
                  []
                );
                console.log('table evenement crée'); 
                let eventData = [{ "name": "WEI", "nb_staff": "35" ,"lieu": "Choix du BDE !!secret!!"},
                 { "name": "Soirée Chartre", "nb_staff": "25" ,"lieu": "Ferme du moulin du bois, Chartres de Bretagne"},
                 { "name": "Soirée bar", "nb_staff": "5" ,"lieu": "Rue de la soif, Rennes"},
                 { "name": "Repas Kfet", "nb_staff": "20" ,"lieu": "Caféteria ENSAI"},
                 { "name": "Barbecue", "nb_staff": "20" ,"lieu": "ENSAI extérieur"}, 
                 { "name": "Gouter", "nb_staff": "10" ,"lieu": "ENSAI"},
                 { "name": "Petit déjeuner", "nb_staff": "10" ,"lieu": "ENSAI"}   
                ];
                let eventQuery = "INSERT INTO evenement (name, nb_staff, lieu) VALUES";
                for (let i = 0; i < eventData.length; ++i) {
                  eventQuery = eventQuery + "('"
                    + eventData[i].name //name
                    + "','"
                    + eventData[i].nb_staff //nb_staff
                    + "','"
                    + eventData[i].lieu //lieu
                    + "')";
                  if (i != eventData.length - 1) {
                    eventQuery = eventQuery + ",";
                  }
                }
                eventQuery = eventQuery + ";";
                console.log('avant requete');
                txn.executeSql(eventQuery, []);
                console.log('apres requete');
                console.log(eventQuery);
              }
            }
          );
        })
        // Alert.alert('SQLite Database and Table Successfully Created...');
        
    };
    return (
        createTable()
        
      );


  }