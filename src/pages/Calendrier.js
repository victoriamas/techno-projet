import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Agenda, LocaleConfig } from 'react-native-calendars'; //bibliothèque pour le calendrier
import { Card, Avatar } from 'react-native-paper';


LocaleConfig.locales['fr'] = {
  monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
  monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
  dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
  dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
  today: 'Aujourd\'hui'
};
LocaleConfig.defaultLocale = 'fr';


const timeToString = (time) => {
  const date = new Date(time);
  return date.toISOString().split('T')[0];
};




function AgendaScreen() {


  const [items, setItems] = useState({});

  const loadItems = (day) => {
    setTimeout(() => {
      for (let i = -5; i < 9; i++) {
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        const strTime = timeToString(time);
        if (!items[strTime]) {
          items[strTime] = [];
          const numItems = 1;
          for (let j = 0; j < numItems; j++) {
            items[strTime].push({
              name: 'Campagne BDE 2021',
              height: Math.max(50, Math.floor(Math.random() * 150)),
            });
          }
        }
      }
      const newItems = {};
      Object.keys(items).forEach((key) => {
        newItems[key] = items[key];
      });
      setItems(newItems);
    }, 1000);
  };

  const renderItem = (item) => {
    return (
      <TouchableOpacity style={{ marginRight: 10, marginTop: 17 }}>
        <Card>
          <Card.Content>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text>{item.name}</Text>
              <Avatar.Text label="BDE" />
            </View>
          </Card.Content>
        </Card>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{ flex: 1 }}>
      <Agenda
        items={items}
        loadItemsForMonth={loadItems}
        selected={'2021-05-21'}
        minDate={'2021-01-01'}
        renderItem={renderItem}
        theme={{
          calendarBackground: '#FFC0CB',
          monthTextColor: '#515F78',
          dayTextColor: '#F7347A',
          textSectionTitleColor: '#F7347A'
        }}
        showScrollIndicator={true}
        markedDates={{
          '2021-04-19': { selected: true, selectedColor: '#F7347A' },
          '2021-05-17': { selected: true, selectedColor: '#F7347A' },
          '2021-06-14': { selected: true, selectedColor: '#F7347A' },
          '2021-09-13': { selected: true, selectedColor: '#F7347A' },
          '2021-04-08': { selected: true, selectedColor: 'green' },
          '2021-05-13': { selected: true, selectedColor: 'green' },
          '2021-06-10': { selected: true, selectedColor: 'green' },
          '2021-09-09': { selected: true, selectedColor: 'green' },
          '2021-05-28': { startingDay: true, selected: true, selectedColor: 'brown' },
          '2021-05-29': { selected: true, selectedColor: 'brown' },
          '2021-05-30': { selected: true, endingDay: true, selectedColor: 'brown' }
        }}
      />
    </View>
  );
};

export default AgendaScreen;